import json
from django.http import JsonResponse
from core.models import MessageModel
from django.contrib.auth.models import User
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def get_user(request):
    qs = User.objects.exclude(id=request.user.id)
    user = list(qs.values())
    return JsonResponse(user, safe=False)


@csrf_exempt
def get_message(request, id):
    q = MessageModel.objects.get(id=id)
    m = dict()
    m['body'] = q.body
    m['timestamp'] = q.timestamp
    m['sender'] = q.sender.username
    m['receiver'] = q.receiver.username
    return JsonResponse(m, safe=False)


@csrf_exempt
def chat(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        receiver = User.objects.get(username=data['recipient'])
        message = MessageModel()
        message.receiver = receiver
        message.sender = request.user
        message.body = data['body']
        message.save()
        return JsonResponse({'message': 'oke'})


@csrf_exempt
def get_conversation(request):
    if request.method == "GET":
        target = request.GET.get("target")
        qs = MessageModel.objects.filter(Q(receiver__username=target, sender=request.user) | Q(
            sender__username=target, receiver=request.user))
        message = list()
        for q in qs:
            m = dict()
            m['body'] = q.body
            m['timestamp'] = q.timestamp
            m['sender'] = q.sender.username
            message.append(m)
        return JsonResponse(message, safe=False)
