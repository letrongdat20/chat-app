from django.urls import path
from django.views.generic import TemplateView
# from rest_framework.routers import DefaultRouter
from core.views import get_message, get_user, chat, get_conversation
from django.contrib.auth.decorators import login_required
# router = DefaultRouter()
# router.register(r'message', MessageModelViewSet, basename='message-api')
# router.register(r'user', UserModelViewSet, basename='user-api')

urlpatterns = [
    path(r'api/v1/user', get_user, name="user"),
    path(r'api/v1/chat/', chat, name='chat'),
    path(r'api/v1/message/<int:id>/', get_message, name="message"),
    path(r'api/v1/message/', get_conversation, name="get_conversation"),
    path('', login_required(TemplateView.as_view(template_name='core/chat.html')), name='home'),
]
