pip install -r requirements.txt
docker run -p 6379:6379 -d redis:5
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
